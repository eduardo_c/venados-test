package com.eduardo.venadostest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eduardo.venadostest.model.Player;
import com.squareup.picasso.Picasso;
import com.test.eduardo.venadostest.R;

import java.util.List;
import java.util.regex.Pattern;

public class PlayersAdapter extends BaseAdapter{

    private Context mContext;
    private List<Player> mPlayers;

    public PlayersAdapter(Context context, List<Player> players) {
        mContext = context;
        mPlayers = players;
    }

    @Override
    public int getCount() {
        return mPlayers.size();
    }

    @Override
    public Object getItem(int i) {
        return mPlayers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Player player = mPlayers.get(i);

        if(view == null){
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            view = layoutInflater.inflate(R.layout.player_item, null);
        }

        Picasso.get().load(player.getPlayerPhoto()).into((ImageView) view.findViewById(R.id.player_item_iv_photo));

        ((TextView) view.findViewById(R.id.player_item_tv_position)).setText(player.getPosition());

        ((TextView) view.findViewById(R.id.player_item_tv_name))
                .setText(String.format(mContext.getString(R.string.player_name), player.getName().split(Pattern.quote(" "))[0], player.getLastName()));

        return view;
    }
}
