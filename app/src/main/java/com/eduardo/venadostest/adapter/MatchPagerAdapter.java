package com.eduardo.venadostest.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class MatchPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> mFragments = new ArrayList<>(2);
    private List<String> mFragmentTitles = new ArrayList<>(2);

    public MatchPagerAdapter(FragmentManager manager){
        super(manager);
    }

    public MatchPagerAdapter(FragmentManager manager, List<Fragment> fragments, List<String> titles){
        super(manager);
        mFragments = fragments;
        mFragmentTitles = titles;
    }

    @Override
    public Fragment getItem(int i) {
        return mFragments.get(i);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitles.get(position);
    }

    public void addFragment(Fragment fragment, String title){
        mFragments.add(fragment);
        mFragmentTitles.add(title);
    }

    public Fragment getFragment(int position) {
        if(position >= mFragments.size())
            return null;
        else
            return mFragments.get(position);
    }
}
