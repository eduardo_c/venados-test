package com.eduardo.venadostest.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eduardo.venadostest.model.Statistic;
import com.squareup.picasso.Picasso;
import com.test.eduardo.venadostest.R;

import java.util.ArrayList;
import java.util.List;

public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.StatisticsViewHolder>{

    private List<Statistic> mStatistics;

    public StatisticsAdapter() {
        mStatistics = new ArrayList<>();
    }

    public StatisticsAdapter(List<Statistic> statistics) {
        mStatistics = statistics;
    }

    public void setItems(List<Statistic> statistics) {
        mStatistics = statistics;
    }

    @NonNull
    @Override
    public StatisticsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.statistics_item, viewGroup, false);
        return new StatisticsAdapter.StatisticsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StatisticsViewHolder statisticsViewHolder, int i) {
        Statistic statistic = mStatistics.get(i);
        statisticsViewHolder.position.setText(String.valueOf(statistic.getPosition()));
        Picasso.get().load(statistic.getImage()).into(statisticsViewHolder.logo);
        statisticsViewHolder.name.setText(statistic.getTeam());
        statisticsViewHolder.matches.setText(String.valueOf(statistic.getGames()));
        statisticsViewHolder.scoreDiff.setText(String.valueOf(statistic.getScoreDiff()));
        statisticsViewHolder.points.setText(String.valueOf(statistic.getPoints()));
    }

    @Override
    public int getItemCount() {
        return mStatistics.size();
    }

    public static class StatisticsViewHolder extends RecyclerView.ViewHolder {
        public TextView position;
        public ImageView logo;
        public TextView name;
        public TextView matches;
        public TextView scoreDiff;
        public TextView points;
        public LinearLayout parent;

        public StatisticsViewHolder(@NonNull View itemView) {
            super(itemView);
            position = itemView.findViewById(R.id.stats_item_tv_position);
            logo = itemView.findViewById(R.id.stats_item_iv_logo);
            name = itemView.findViewById(R.id.stats_item_tv_team_name);
            matches = itemView.findViewById(R.id.stats_item_tv_matches);
            scoreDiff = itemView.findViewById(R.id.stats_item_tv_score_diff);
            points = itemView.findViewById(R.id.stats_item_tv_points);
            parent = itemView.findViewById(R.id.stats_item_ly_parent);
        }
    }

}
