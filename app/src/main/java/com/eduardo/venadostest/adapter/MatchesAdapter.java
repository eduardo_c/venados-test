package com.eduardo.venadostest.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.eduardo.venadostest.model.Game;
import com.squareup.picasso.Picasso;
import com.test.eduardo.venadostest.R;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.Month;

import java.util.List;
import java.util.regex.Pattern;

public class MatchesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final String VENADOS_LOGO_URL = "https://s3.amazonaws.com/lmxwebsite/docs/archdgtl/AfldDrct/logos/10732/10732.png";
    private static final int HEADER_VIEW_TYPE = 1;
    private static final int GAME_VIEW_TYPE = 2;

    private List<Game> mGames;
    private OnCalendarClickListener mListener;

    public static class MatchViewHolder extends RecyclerView.ViewHolder {
        public ImageView calendar;
        public TextView date;
        public ImageView localTeamLogo;
        public TextView localTeamName;
        public TextView score;
        public ImageView awayTeamLogo;
        public TextView awayTeamName;

        public MatchViewHolder(LinearLayout ly){
            super(ly);
            calendar = ly.findViewById(R.id.match_item_iv_calendar);
            date = ly.findViewById(R.id.match_item_tv_date);
            localTeamLogo = ly.findViewById(R.id.match_item_iv_local);
            localTeamName = ly.findViewById(R.id.match_item_tv_local);
            score = ly.findViewById(R.id.match_item_tv_score);
            awayTeamLogo = ly.findViewById(R.id.match_item_iv_away);
            awayTeamName = ly.findViewById(R.id.match_item_tv_away);
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder{
        public TextView month;

        public HeaderViewHolder(LinearLayout ly){
            super(ly);
            month = ly.findViewById(R.id.header_match_item_month);
        }
    }

    public MatchesAdapter(List<Game> games){
        setItems(games);
    }

    @Override
    public int getItemViewType(int position) {
        if(mGames.get(position) == null)
            return HEADER_VIEW_TYPE;
        else
            return GAME_VIEW_TYPE;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if(i == HEADER_VIEW_TYPE) {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_match_item, viewGroup, false);
            return new HeaderViewHolder((LinearLayout) view);
        }
        else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_match_item, viewGroup, false);
            return new MatchViewHolder((LinearLayout) view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        final Game game = mGames.get(i);
        if(viewHolder.getItemViewType() == HEADER_VIEW_TYPE){
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            Game nextGameItem = mGames.get(i + 1);
            Month month = LocalDateTime.parse(nextGameItem.getMatchDate().split(Pattern.quote("+"))[0]).getMonth();
            headerViewHolder.month.setText(month.name());
        }
        else {
            MatchViewHolder matchViewHolder = (MatchViewHolder) viewHolder;
            final LocalDateTime date = LocalDateTime.parse(game.getMatchDate().split(Pattern.quote("+"))[0]);
            matchViewHolder.date.setText(String.format("%d \n %s", date.getDayOfMonth(), date.getDayOfWeek().name()));
            if (game.getLocal()) {
                Picasso.get().load(VENADOS_LOGO_URL).into(matchViewHolder.localTeamLogo);
                Picasso.get().load(game.getOpponentImage()).into(matchViewHolder.awayTeamLogo);
                matchViewHolder.awayTeamName.setText(game.getOpponent());
            } else {
                Picasso.get().load(game.getOpponentImage()).into(matchViewHolder.localTeamLogo);
                Picasso.get().load(VENADOS_LOGO_URL).into(matchViewHolder.awayTeamLogo);
                matchViewHolder.localTeamName.setText(game.getOpponent());
            }
            matchViewHolder.score.setText(String.format("%d - %d", game.getHomeScore(), game.getAwayScore()));

            matchViewHolder.calendar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onCalendarCLick(date, game.getLocal(), game.getOpponent());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mGames.size();
    }

    public void setItems(List<Game> games){
        mGames = games;
        if(mGames.size() == 0)
            return;
        LocalDateTime date = LocalDateTime.parse(mGames.get(0).getMatchDate().split(Pattern.quote("+"))[0]);
        Month auxMonth = date.getMonth();
        Month auxMonth2;
        mGames.add(0, null);
        for(int i = 0; i < mGames.size(); i++){
            Game game = mGames.get(i);
            if(game != null){
                auxMonth2 = LocalDateTime.parse(game.getMatchDate().split(Pattern.quote("+"))[0]).getMonth();
                if(auxMonth != auxMonth2){
                    mGames.add(i, null);
                    auxMonth = auxMonth2;
                }
            }
        }
    }

    public void setOnCalendarClickListener(OnCalendarClickListener listener){
        mListener = listener;
    }

    public interface OnCalendarClickListener {
        void onCalendarCLick(LocalDateTime matchDate, boolean local, String opponent);
    }
}
