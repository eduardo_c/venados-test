package com.eduardo.venadostest.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.eduardo.venadostest.adapter.StatisticsAdapter;
import com.eduardo.venadostest.model.Game;
import com.eduardo.venadostest.model.Statistic;
import com.eduardo.venadostest.util.StatisticsDeserializer;
import com.eduardo.venadostest.ws.VenadosService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.eduardo.venadostest.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class StatisticsFragment extends Fragment implements Callback<List<Statistic>>{

    private static final String LOG_TAG = StatisticsFragment.class.getSimpleName();
    private static final String BASE_URL = "https://venados.dacodes.mx/api/";
    private static final String PARAM_STATISTICS = "param_statistics";
    private static final short STATE_LOADING = 0;
    private static final short STATE_LOADED_SUCCESS = 1;
    private static final short STATE_LOADED_FAILED = 2;

    private RecyclerView mRecyclerView;
    private List<Statistic> mStatistics;
    private RecyclerView.LayoutManager mLayoutManager;
    private StatisticsAdapter mAdapter;
    private ProgressBar mPb;

    public StatisticsFragment() {

    }

    public StatisticsFragment newInstance(Statistic[] statistics) {
        StatisticsFragment fragment = new StatisticsFragment();
        Bundle args = new Bundle();
        args.putParcelableArray(PARAM_STATISTICS, statistics);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null)
            mStatistics = Arrays.asList((Statistic[]) getArguments().getParcelableArray(PARAM_STATISTICS));
        else
            mStatistics = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.fragment_stats_recyclerview);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mPb = view.findViewById(R.id.fragment_stats_pb);
        if (mStatistics.isEmpty()) {
            setLoadingState(STATE_LOADING);
            mAdapter = new StatisticsAdapter();
            getStatisticsAsync();
        }
        else {
            setLoadingState(STATE_LOADED_SUCCESS);
            mAdapter = new StatisticsAdapter(mStatistics);
            mRecyclerView.setAdapter(mAdapter);
        }
    }

    private void getStatisticsAsync(){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(List.class, new StatisticsDeserializer())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss+Z")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        VenadosService service = retrofit.create(VenadosService.class);

        Call<List<Statistic>> call = service.getStatistics();
        call.enqueue(StatisticsFragment.this);
    }

    @Override
    public void onResponse(Call<List<Statistic>> call, Response<List<Statistic>> response) {
        setLoadingState(STATE_LOADED_SUCCESS);
        mStatistics = response.body();
        mAdapter.setItems(mStatistics);
        if(mRecyclerView.getAdapter() == null) {
            mRecyclerView.setAdapter(mAdapter);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFailure(Call<List<Statistic>> call, Throwable t) {
        setLoadingState(STATE_LOADED_FAILED);
        t.printStackTrace();
    }

    private void setLoadingState(short state) {
        switch (state) {
            case STATE_LOADING:
                mPb.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
                break;
            case STATE_LOADED_SUCCESS:
                mPb.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                break;
            case STATE_LOADED_FAILED:
                //TODO add load fail view and make it visible
                mPb.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
        }
    }
}
