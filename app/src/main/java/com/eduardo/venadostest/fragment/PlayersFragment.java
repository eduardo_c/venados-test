package com.eduardo.venadostest.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.eduardo.venadostest.adapter.PlayersAdapter;
import com.eduardo.venadostest.dialog.PlayerDetailsDialog;
import com.eduardo.venadostest.model.Player;
import com.eduardo.venadostest.model.Statistic;
import com.eduardo.venadostest.util.PlayersDeserializer;
import com.eduardo.venadostest.ws.VenadosService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.eduardo.venadostest.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlayersFragment extends Fragment implements Callback<List<Player>>, AdapterView.OnItemClickListener{
    private static final String BASE_URL = "https://venados.dacodes.mx/api/";
    private static final String DIALOG_FRAGMENT_PLAYER_TAG = "dialog";
    private static final int STATE_LOADING = 0;
    private static final int STATE_LOAD_SUCCESS = 1;
    private static final int STATE_LOAD_FAIL = 2;

    private GridView mGridView;
    private PlayersAdapter mAdapter;
    private ProgressBar mPb;

    public PlayersFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_players, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mGridView = view.findViewById(R.id.fragment_players_gridview);
        mPb = view.findViewById(R.id.fragment_players_pb);
        getPlayersAsync();
    }

    private void getPlayersAsync(){
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(List.class, new PlayersDeserializer())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss+Z")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        VenadosService service = retrofit.create(VenadosService.class);

        Call<List<Player>> call = service.getPlayers();
        call.enqueue(PlayersFragment.this);
    }

    private void setLoadingState(int state){
        switch (state){
            case STATE_LOADING:
                mPb.setVisibility(View.VISIBLE);
                mGridView.setVisibility(View.GONE);
                break;
            case STATE_LOAD_SUCCESS:
                mPb.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
                break;
            case STATE_LOAD_FAIL:
                //TODO add and show a view for fail message
                mPb.setVisibility(View.GONE);
                mGridView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResponse(Call<List<Player>> call, Response<List<Player>> response) {
        setLoadingState(STATE_LOAD_SUCCESS);
        List<Player> players = response.body();
        mAdapter = new PlayersAdapter(getActivity(), players);
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(PlayersFragment.this);
    }

    @Override
    public void onFailure(Call<List<Player>> call, Throwable t) {
        setLoadingState(STATE_LOAD_FAIL);
        t.printStackTrace();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        showPlayerDetailsFragment((Player) mGridView.getAdapter().getItem(i));
    }

    private void showPlayerDetailsFragment(Player player) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Fragment prev = getFragmentManager().findFragmentByTag(DIALOG_FRAGMENT_PLAYER_TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFragment = PlayerDetailsDialog.newInstance(player);
        dialogFragment.show(ft, DIALOG_FRAGMENT_PLAYER_TAG);
    }
}
