package com.eduardo.venadostest.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.eduardo.venadostest.adapter.MatchPagerAdapter;
import com.eduardo.venadostest.adapter.MatchesAdapter;
import com.eduardo.venadostest.model.Game;
import com.eduardo.venadostest.util.GamesDeserializer;
import com.eduardo.venadostest.ws.VenadosService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.eduardo.venadostest.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment
        implements TabLayout.OnTabSelectedListener, Callback<List<Game>>, MatchesFragment.OnSwipeRefreshMatchesListener{

    private static final String BASE_URL = "https://venados.dacodes.mx/api/";
    private static final String LOG_TAG = HomeFragment.class.getSimpleName();

    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private MatchesFragment mCupMatchFragment;
    private MatchesFragment mLeagueMatchFragment;
    private ProgressDialog mPd;

    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewPager = view.findViewById(R.id.fragment_home_view_pager);
        mTabLayout = view.findViewById(R.id.fragment_home_tab);

        initializeFragments(new Game[]{}, new Game[]{});

        mTabLayout.addOnTabSelectedListener(HomeFragment.this);


        mTabLayout.addTab(mTabLayout.newTab().setText(MatchType.CUP.toString()));
        mTabLayout.addTab(mTabLayout.newTab().setText(MatchType.LEAGUE.toString()));

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        mPd = new ProgressDialog(getActivity());

        getGamesAsync(true);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        Fragment cupFfragment = ((MatchPagerAdapter) mViewPager.getAdapter()).getFragment(0);
        Fragment leagueFfragment = ((MatchPagerAdapter) mViewPager.getAdapter()).getFragment(1);
        getActivity().getSupportFragmentManager().beginTransaction().remove(leagueFfragment).commit();
        getActivity().getSupportFragmentManager().beginTransaction().remove(cupFfragment).commit();
    }

    private void getGamesAsync(boolean showLoader){
        if(showLoader) {
            mPd.setMessage(getResources().getString(R.string.fetching_games));
            mPd.show();
        }

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(List.class, new GamesDeserializer())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss+Z")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        VenadosService service = retrofit.create(VenadosService.class);

        Call<List<Game>> call = service.getGames();
        call.enqueue(HomeFragment.this);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onResponse(Call<List<Game>> call, Response<List<Game>> response) {
        mPd.dismiss();
        if(response.isSuccessful()){
            List<Game> games = response.body();
            List<Game> cupGames = new ArrayList<>();
            List<Game> leagueGames = new ArrayList<>();
            for(Game game : games) {
                if (game.getLeague().toUpperCase().equals(MatchType.CUP.toString())) {
                    cupGames.add(game);
                } else if (game.getLeague().toUpperCase().equals(MatchType.LEAGUE.toString())) {
                    leagueGames.add(game);
                }
            }
            mCupMatchFragment.setGames(cupGames);
            mLeagueMatchFragment.setGames(leagueGames);
        }
        else{
            Log.e(LOG_TAG, response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<List<Game>> call, Throwable t) {
        mPd.dismiss();
        t.printStackTrace();
    }

    @Override
    public void onRefreshMatches() {
        getGamesAsync(false);
    }

    public enum MatchType {
        LEAGUE("ASCENSO MX"),
        CUP("COPA MX");

        private final String description;

        private MatchType(String description){
            this.description = description;
        }

        @Override
        public String toString() {
            return description;
        }
    }

    private void initializeFragments(Game[] cupGames, Game[] leagueGames){
        mCupMatchFragment = MatchesFragment.newInstance(cupGames);
        mLeagueMatchFragment = MatchesFragment.newInstance(leagueGames);
        MatchPagerAdapter adapter = new MatchPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(mCupMatchFragment, MatchType.CUP.name());
        adapter.addFragment(mLeagueMatchFragment, MatchType.LEAGUE.name());

        mViewPager.setAdapter(adapter);

        mCupMatchFragment.setOnSwipeRefreshListener(this);
        mLeagueMatchFragment.setOnSwipeRefreshListener(this);
    }
}
