package com.eduardo.venadostest.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.eduardo.venadostest.adapter.MatchesAdapter;

import com.eduardo.venadostest.model.Game;
import com.test.eduardo.venadostest.R;

import org.threeten.bp.LocalDateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class MatchesFragment extends Fragment implements MatchesAdapter.OnCalendarClickListener{

    private static final String PARAM_GAMES = "param_mgames";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MatchesAdapter mAdapter;
    private List<Game> mGames;
    private OnSwipeRefreshMatchesListener mListener;

    public MatchesFragment() {

    }

    public static MatchesFragment newInstance(Game[] games) {
        MatchesFragment fragment = new MatchesFragment();
        Bundle args = new Bundle();
        args.putParcelableArray(PARAM_GAMES, games);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGames = Arrays.asList((Game[]) getArguments().getParcelableArray(PARAM_GAMES));
        }
        else {
            mGames = new ArrayList<>();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_matches, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRecyclerView = view.findViewById(R.id.fragment_matches_rv);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new MatchesAdapter(mGames);
        mAdapter.setOnCalendarClickListener(MatchesFragment.this);
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = view.findViewById(R.id.fragment_matches_swipe);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                if(mListener != null)
                    mListener.onRefreshMatches();
            }
        });
    }

    public void setGames(List<Game> games){
        mSwipeRefreshLayout.setRefreshing(false);
        mGames = games;
        mAdapter.setItems(mGames);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCalendarCLick(LocalDateTime matchDate, boolean local, String opponent) {
        //TODO schedule date in calendar
        //Toast.makeText(getActivity(), matchDate.toString(), Toast.LENGTH_SHORT).show();
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(matchDate.getYear(), matchDate.getMonthValue(), matchDate.getDayOfMonth(), matchDate.getHour(), matchDate.getMinute());
        String match;

        if(local)
            match = String.format("%s vs %s", getResources().getString(R.string.venados), opponent);
        else
            match = String.format("%s vs %s", opponent, getResources().getString(R.string.venados));

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, getResources().getString(R.string.fragments_matches_event_title))
                .putExtra(CalendarContract.Events.DESCRIPTION, match)
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);

        if(local)
            intent.putExtra(CalendarContract.Events.EVENT_LOCATION,  getResources().getString(R.string.venados_stadium));

        beginTime.add(Calendar.HOUR, 2);

        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, beginTime);

        startActivity(intent);
    }

    public void setOnSwipeRefreshListener(OnSwipeRefreshMatchesListener listener){
        mListener = listener;
    }

    public interface OnSwipeRefreshMatchesListener{
        void onRefreshMatches();
    }
}
