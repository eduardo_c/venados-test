package com.eduardo.venadostest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Player implements Parcelable{

    @SerializedName("image")
    private String playerPhoto;

    @SerializedName("name")
    private String name;

    @SerializedName("first_surname")
    private String lastName;

    @SerializedName("second_surname")
    private String secondLastName;

    @SerializedName("birthday")
    private String birthday;

    @SerializedName("birth_place")
    private String birthPlace;

    @SerializedName("weight")
    private int weight;

    @SerializedName("height")
    private float height;

    @SerializedName("position")
    private String position;

    @SerializedName("last_team")
    private String lastTeam;

    public Player(Parcel in) {
        super();
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>(){

        @Override
        public Player createFromParcel(Parcel parcel) {
            return new Player(parcel);
        }

        @Override
        public Player[] newArray(int i) {
            return new Player[i];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(playerPhoto);
        parcel.writeString(name);
        parcel.writeString(lastName);
        parcel.writeString(secondLastName);
        parcel.writeString(birthday);
        parcel.writeString(birthPlace);
        parcel.writeInt(weight);
        parcel.writeFloat(height);
        parcel.writeString(position);
        parcel.writeString(lastTeam);
    }

    private void readFromParcel(Parcel in) {
        playerPhoto = in.readString();
        name = in.readString();
        lastName = in.readString();
        secondLastName = in.readString();
        birthday = in.readString();
        birthPlace = in.readString();
        weight = in.readInt();
        height = in.readFloat();
        position= in.readString();
        lastTeam = in.readString();
    }
}
