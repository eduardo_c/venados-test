package com.eduardo.venadostest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Statistic implements Parcelable{

    @SerializedName("position")
    private short position;

    @SerializedName("image")
    private String image;

    @SerializedName("team")
    private String team;

    @SerializedName("games")
    private short games;

    @SerializedName("score_diff")
    private int scoreDiff;

    @SerializedName("points")
    private int points;

    public Statistic(Parcel parcel){
        super();
        readFromParcel(parcel);
    }

    public static final Parcelable.Creator<Statistic> CREATOR = new Parcelable.Creator<Statistic>() {

        @Override
        public Statistic createFromParcel(Parcel parcel) {
            return new Statistic(parcel);
        }

        @Override
        public Statistic[] newArray(int i) {
            return new Statistic[i];
        }
    };

    private void readFromParcel(Parcel in) {
        position = in.readByte();
        image = in.readString();
        team = in.readString();
        games = in.readByte();
        scoreDiff = in.readInt();
        points = in.readInt();

    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) position);
        parcel.writeString(image);
        parcel.writeString(team);
        parcel.writeByte((byte) games);
        parcel.writeInt(scoreDiff);
        parcel.writeInt(points);
    }
}
