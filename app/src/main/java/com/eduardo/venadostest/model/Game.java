package com.eduardo.venadostest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Game implements Parcelable{
    @SerializedName("local")
    private Boolean local;

    @SerializedName("opponent")
    private String opponent;

    @SerializedName("opponent_image")
    private String opponentImage;

    @SerializedName("datetime")
    private String matchDate;

    @SerializedName("league")
    private String league;

    @SerializedName("image")
    private String image;

    @SerializedName("home_score")
    private short homeScore;

    @SerializedName("away_score")
    private short awayScore;

    public Game(Parcel in){
        super();
        readFromParcel(in);
    }

    public static final Parcelable.Creator<Game> CREATOR = new Parcelable.Creator<Game>(){

        @Override
        public Game createFromParcel(Parcel parcel) {
            return new Game(parcel);
        }

        @Override
        public Game[] newArray(int i) {
            return new Game[i];
        }
    };

    public void readFromParcel(Parcel in){
        local = in.readByte() != 0;
        opponent = in.readString();
        opponentImage = in.readString();
        matchDate = in.readString();
        league = in.readString();
        image = in.readString();
        homeScore = in.readByte();
        awayScore = in.readByte();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (this.local ? 1 : 0));
        parcel.writeString(this.opponent);
        parcel.writeString(this.opponentImage);
        parcel.writeString(this.matchDate);
        parcel.writeString(this.league);
        parcel.writeString(this.image);
        parcel.writeByte((byte) this.homeScore);
        parcel.writeByte((byte) this.awayScore);
    }
}
