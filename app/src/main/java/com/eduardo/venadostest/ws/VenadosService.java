package com.eduardo.venadostest.ws;

import com.eduardo.venadostest.model.Game;
import com.eduardo.venadostest.model.Player;
import com.eduardo.venadostest.model.Statistic;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;

public interface VenadosService {

    @Headers({"Accept: application/json"})
    @GET("games")
    Call<List<Game>> getGames();

    @Headers({"Accept: application/json"})
    @GET("statistics")
    Call<List<Statistic>> getStatistics();

    @Headers({"Accept: application/json"})
    @GET("players")
    Call<List<Player>> getPlayers();

}
