package com.eduardo.venadostest.util;

import com.eduardo.venadostest.model.Game;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GamesDeserializer implements JsonDeserializer<List<Game>>{

    @Override
    public List<Game> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<Game> list = new ArrayList<>();
        Type valueType = ((ParameterizedType) typeOfT).getActualTypeArguments()[0];

        JsonElement data = json.getAsJsonObject().get("data");
        JsonElement games = data.getAsJsonObject().get("games");
        for(JsonElement game : games.getAsJsonArray()){
            list.add((Game) context.deserialize(game, valueType));
        }

        return list;
    }
}
