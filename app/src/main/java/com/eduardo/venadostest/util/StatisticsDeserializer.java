package com.eduardo.venadostest.util;

import com.eduardo.venadostest.model.Game;
import com.eduardo.venadostest.model.Statistic;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class StatisticsDeserializer implements JsonDeserializer<List<Statistic>> {

    @Override
    public List<Statistic> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<Statistic> list = new ArrayList<>();
        Type valueType = ((ParameterizedType) typeOfT).getActualTypeArguments()[0];

        JsonElement data = json.getAsJsonObject().get("data");
        JsonElement games = data.getAsJsonObject().get("statistics");
        for(JsonElement statistic : games.getAsJsonArray()){
            list.add((Statistic) context.deserialize(statistic, valueType));
        }

        return list;
    }
}
