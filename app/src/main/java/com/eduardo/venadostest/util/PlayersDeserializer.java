package com.eduardo.venadostest.util;

import com.eduardo.venadostest.model.Player;
import com.eduardo.venadostest.model.Statistic;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class PlayersDeserializer implements JsonDeserializer<List<Player>> {
    @Override
    public List<Player> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        List<Player> list = new ArrayList<>();
        Type valueType = ((ParameterizedType) typeOfT).getActualTypeArguments()[0];

        JsonElement data = json.getAsJsonObject().get("data");
        JsonElement team = data.getAsJsonObject().get("team");
        JsonElement forwards = team.getAsJsonObject().get("forwards");
        JsonElement centers = team.getAsJsonObject().get("centers");
        JsonElement defenses = team.getAsJsonObject().get("defenses");
        JsonElement goalkeepers = team.getAsJsonObject().get("goalkeepers");
        for(JsonElement player : forwards.getAsJsonArray()){
            list.add((Player) context.deserialize(player, valueType));
        }
        for(JsonElement player : centers.getAsJsonArray()){
            list.add((Player) context.deserialize(player, valueType));
        }
        for(JsonElement player : defenses.getAsJsonArray()){
            list.add((Player) context.deserialize(player, valueType));
        }
        for(JsonElement player : goalkeepers.getAsJsonArray()){
            list.add((Player) context.deserialize(player, valueType));
        }

        return list;
    }
}
