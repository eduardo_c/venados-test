package com.eduardo.venadostest.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.eduardo.venadostest.model.Player;
import com.squareup.picasso.Picasso;
import com.test.eduardo.venadostest.R;

import org.threeten.bp.LocalDate;
import org.threeten.bp.LocalDateTime;
import org.w3c.dom.Text;

import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlayerDetailsDialog extends DialogFragment {

    private static final String PARAM_PLAYER = "param_player";
    private Player mPLayer;

    public PlayerDetailsDialog(){

    }

    public static PlayerDetailsDialog newInstance(Player player) {
        PlayerDetailsDialog dialog = new PlayerDetailsDialog();
        Bundle args = new Bundle();
        args.putParcelable(PARAM_PLAYER, player);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            mPLayer = getArguments().getParcelable(PARAM_PLAYER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_player_details, container, false);

        if(mPLayer != null){
            Picasso.get().load(mPLayer.getPlayerPhoto()).into((ImageView) view.findViewById(R.id.dialog_player_photo));
            ((TextView) view.findViewById(R.id.dialog_player_name))
                    .setText(String.format(getString(R.string.player_name),
                                    mPLayer.getName().split(Pattern.quote(" "))[0],
                                    mPLayer.getLastName()));
            ((TextView) view.findViewById(R.id.dialog_player_position)).setText(mPLayer.getPosition());

            LocalDate date = LocalDate.parse(mPLayer.getBirthday().split(Pattern.quote("T"))[0]);
            ((TextView) view.findViewById(R.id.dialog_player_birthday))
                    .setText(String
                            .format(getString(R.string.dialog_player_birthday_placeholder),
                                    String.valueOf(date.getDayOfMonth()),
                                    String.valueOf(date.getMonth().getValue()),
                                    String.valueOf(date.getYear())));
            ((TextView) view.findViewById(R.id.dialog_player_birth_place)).setText(mPLayer.getBirthPlace());
            ((TextView) view.findViewById(R.id.dialog_player_weight)).setText(String.valueOf(mPLayer.getWeight()));
            ((TextView) view.findViewById(R.id.dialog_player_height)).setText(String.valueOf(mPLayer.getHeight()));
            ((TextView) view.findViewById(R.id.dialog_player_last_team)).setText(mPLayer.getLastTeam());
        }

        return view;
    }
}
